import request from "../utils/request";

// 获取轮播图列表
export const loginApi = (code) => {
  return request(`/weixin/wxLogin/${code}`);
};

// 获取用户信息列表
export const getUserInfoApi = () => {
  return request(`/weixin/getuserInfo`);
};
