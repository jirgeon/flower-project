import request from "../utils/request";

// 1. 获取默认地址
export const getDefaultAddressApi = () => {
  return request("/userAddress/getOrderAddress");
};

// 2. 获取地址列表
export const getAddressListApi = () => {
  return request("/userAddress/findUserAddress");
};

// 3. 新增地址
export const addAddressApi = (data) => {
  return request("/userAddress/save", data, "POST");
};

// 4. 修改地址
export const updateAddressApi = (data) => {
  return request("/userAddress/update", data, "POST");
};

// 5. 删除一个地址
export const delAddressApi = (id) => {
  return request(`/userAddress/delete/${id}`);
};

// 6. 根据 id 获取地址
export const getAddressByIdApi = (id) => {
  return request(`/userAddress/${id}`);
};
