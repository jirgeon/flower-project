import request from "../utils/request";

// 1. 获取购物车列表
export const getCartListApi = () => {
  return request("/cart/getCartList");
};

// 2. 增减购物车商品数量
export const upDateCartCountApi = (goodsId, count) => {
  return request(`/cart/addToCart/${goodsId}/${count}`);
};

// 3. 全选购物车商品
export const checkAllCartApi = (isChecked) => {
  return request(`/cart/checkAllCart/${isChecked}`);
};

// 4. 单选购物车商品
export const checkCartApi = (goodsId, isChecked) => {
  return request(`/cart/checkCart/${goodsId}/${isChecked}`);
};

// 5.删除购物车商品
export const delCartApi = (goodsId) => {
  return request(`/cart/delete/${goodsId}`);
};
