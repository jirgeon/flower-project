import request from "../utils/request";

// 获取商品列表数据
export const getGoodsListApi = ({ page, limit, ...data }) => {
  return request(`/goods/list/${page}/${limit}`, data);
};

// 获取商品详情数据
export const getGoodsDetailInfoApi = (goodsId) => {
  return request(`/goods/${goodsId}`);
};
