import request from "../utils/request";

// 获取轮播图列表
export const getBannerListApi = () => {
  return request("/index/findBanner");
};

// 获取分类导航
export const getNavListApi = () => {
  return request("/index/findCategory1");
};

// 获取猜你喜欢列表
export const getLikeGoodsListApi = () => {
  return request("/index/findListGoods");
};

// 获取人气推荐列表
export const getRecommendGoodsListApi = () => {
  return request("/index/findRecommendGoods");
};
