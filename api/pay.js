import request from "../utils/request";

// 1. 立即购买 —— 获取商品列表信息
export const getGoodsInfoByBuyNowApi = (goodsId, blessing) => {
  return request(`/order/buy/${goodsId}`, { blessing });
};

// 2. 获取购物订单 —— 获取商品列表信息
export const getGoodsInfoByTradeApi = () => {
  return request(`/order/trade`);
};

// 3. 提交订单接口 : 提交给服务器生成订单 返回订单编号
export const submitOrderApi = (data) => {
  return request(`/order/submitOrder`, data, "POST");
};

// 4. 生成微信订单，得到微信支付参数
export const createWeixinOrderApi = (orderNo) => {
  return request(`/webChat/createJsapi/${orderNo}`);
};
