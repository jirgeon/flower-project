import request from "../utils/request";

// 获取轮播图列表
export const getCategoryListApi = () => {
  return request("/index/findCategoryTree");
};
