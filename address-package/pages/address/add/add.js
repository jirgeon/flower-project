// pages/address/add/add.js
import {
  addAddressApi,
  getAddressByIdApi,
  updateAddressApi,
} from "../../../../api/address";
Page({
  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    // 姓名
    name: "",
    // 电话
    phone: "",
    // 标签
    tagName: "家",
    // 用户地址
    address: "",
    // 省
    provinceCode: "",
    // 市
    cityCode: "",
    // 区
    districtCode: "",
    // 是否是默认地址
    isDefault: 0,
    // 用户选择地址 省/市/县
    region: "",
    // 地址id，用于修改地址跳转过来的地址id
    id: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.data.id = options.id || "";
  },

  onShow() {
    if (!this.data.id) return;
    this.getAddress();
  },

  async getAddress() {
    const {
      name,
      phone,
      provinceName,
      cityName,
      districtName,
      provinceCode,
      cityCode,
      districtCode,
      address,
      tagName,
      isDefault,
    } = await getAddressByIdApi(this.data.id);

    const region = [provinceName, cityName, districtName];

    this.setData({
      name,
      phone,
      region,
      address,
      tagName,
      isDefault,
      provinceCode,
      cityCode,
      districtCode,
    });
  },

  // 设置地址
  setRegion(e) {
    // console.log(e);
    this.setData({
      region: e.detail.value,
      provinceCode: e.detail.code[0],
      cityCode: e.detail.code[1],
      districtCode: e.detail.code[2],
    });
  },

  // 设置默认地址
  setDefault(e) {
    this.setData({
      isDefault: +e.detail.value, // 利用隐式转换的原则 true 转换成数字 1， false 转换成 0
    });
    // console.log(e.detail.value);
  },

  // 新增地址
  async save() {
    const {
      id,
      name,
      phone,
      region,
      address,
      tagName,
      isDefault,
      provinceCode,
      cityCode,
      districtCode,
    } = this.data;

    if (!name) {
      wx.showToast({
        title: "请输入您的姓名",
        icon: "error",
      });
      return;
    }
    if (!phone) {
      wx.showToast({
        title: "请输入您的手机",
        icon: "error",
      });
      return;
    }
    const phoneReg = /^1[3-9][0-9]{9}$/;
    if (!phoneReg.test(phone)) {
      wx.showToast({
        title: "格式错误",
        icon: "error",
      });
      return;
    }
    if (!region.length) {
      wx.showToast({
        title: "请选择你的省/市/区",
        icon: "error",
      });
      return;
    }
    if (!address) {
      wx.showToast({
        title: "请输入你的详细地址",
        icon: "error",
      });
      return;
    }

    if (!id) {
      // 添加
      await addAddressApi({
        name,
        phone,
        address,
        tagName,
        isDefault,
        provinceCode,
        cityCode,
        districtCode,
      });
    } else {
      // 修改
      await updateAddressApi({
        id,
        name,
        phone,
        address,
        tagName,
        isDefault,
        provinceCode,
        cityCode,
        districtCode,
      });
    }

    wx.showToast({
      title: `${id ? "修改" : "新增"}成功`,
      icon: "success",
      success() {
        setTimeout(() => {
          // 添加成功后返回地址列表页
          wx.navigateBack();
        }, 600);
      },
    });
  },
});
