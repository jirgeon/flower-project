// pages/address/list/list.js
import {
  getAddressListApi,
  delAddressApi,
  getAddressByIdApi,
} from "../../../../api/address";

const app = getApp();

Page({
  data: {
    addressList: [],
  },

  onLoad(options) {},
  onShow() {
    this.getAddressList();
  },

  // 获取地址列表
  async getAddressList() {
    const addressList = await getAddressListApi();
    this.setData({
      addressList,
    });
  },

  // 删除一个地址
  delAddressHandler(e) {
    wx.showModal({
      title: "删除",
      content: "是否要删除该地址",
      complete: async (res) => {
        if (res.confirm) {
          await delAddressApi(e.currentTarget.dataset.id);
          this.getAddressList();
        }
      },
    });
  },

  // 设置送货地址
  async setAddressHandler(e) {
    const res = await getAddressByIdApi(e.currentTarget.dataset.id);
    app.globalData.address = res;
    wx.navigateBack();
  },

  goEdit(e) {
    console.log("执行");
    wx.navigateTo({
      url: `/address-package/pages/address/add/add?id=${e.currentTarget.dataset.id}`,
    });
  },
});
