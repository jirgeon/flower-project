/**
 * 封装请求函数
 */

const app = getApp();
const baseUrl = "https://gmall-prod.atguigu.cn/mall-api";

function request(url, data = {}, method = "GET") {
  return new Promise((resovle, reject) => {
    wx.request({
      url: baseUrl + url,
      data,
      method,
      header: {
        token: app.globalData.token || "",
      },
      success(res) {
        // console.log(res);
        if (res.statusCode >= 200 && res.statusCode < 300) {
          // 请求成功
          if (res.data.code === 200) {
            // 功能请求成功
            resovle(res.data.data);
          } else {
            wx.showToast({
              title: res.data.message,
              icon: "error",
            });
            reject(res.data.message);
          }
        } else {
          // 请求失败
          wx.showToast({
            title: res.data.message,
            icon: "error",
          });
          reject(res.data.message);
        }
      },
      fail(err) {
        // 找不到目标服务器，或请求超时触发
        console.log(err);
        wx.showToast({
          title: err.errMsg,
          icon: "error",
        });
        reject(err.errMsg);
      },
    });
  });
}

export default request;
