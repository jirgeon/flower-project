// 节流函数
export function throttle(fn, time) {
  let startTime = 0;
  return function () {
    let endTime = Date.now();
    if (endTime - startTime > time) {
      fn.apply(this, arguments);
      startTime = endTime;
    }
  };
}

// 防抖函数
export function debounce(fn, time) {
  let timer;
  return function () {
    clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(this, arguments);
    }, time);
  };
}

// 处理日期函数封装
export function formatData(time) {
  const date = new Date(time);

  const y = date.getFullYear();
  let m = date.getMonth() + 1;
  m = m < 10 ? "0" + m : m;
  let d = date.getDate();
  d = d < 10 ? "0" + d : d;

  return `${y}-${m}-${d}`;
}
