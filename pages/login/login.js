// pages/login/login.js
import { loginApi } from "../../api/user";

// 获取 app 实例，访问全局数据
const app = getApp();

Page({
  data: {},

  // 登录功能
  login() {
    // 1. wx.login 获取 code （用户登录凭证）
    wx.login({
      success: async (res) => {
        const { code } = res;
        // 2. 小程序客户端，携带 code 发送给 我们的后端接口
        const { token } = await loginApi(code);
        // 3. 将获取到的 token 持久化存储
        wx.setStorageSync("token", token);
        // 再存储到内存中（方便使用时快速读取）
        app.globalData.token = token;
        // 最后登录成功后返回首页
        wx.switchTab({
          url: "/pages/user/user",
        });
      },
    });
  },

  onLoad(options) {},

  onReady() {},

  onShow() {},
});
