import {
  getCartListApi,
  checkCartApi,
  checkAllCartApi,
  delCartApi,
  upDateCartCountApi,
} from "../../api/cart";
import { throttle, debounce } from "../../utils/tools";

const app = getApp();

Page({
  data: {
    cartList: [],
    isAllCheck: false,
    totalPrice: 0,
    totalCount: 0,
    isLogin: false,
  },

  onShow() {
    const { token } = app.globalData;
    if (!token) return;
    this.getCartList();
    this.setData({
      isLogin: true,
    });
  },

  // 获取购物车列表
  async getCartList() {
    const cartList = await getCartListApi();

    // 设置是否全选中
    const isAllCheck = cartList.every((item) => item.isChecked === 1);
    // 计算总价
    const totalPrice = cartList.reduce((p, c) => {
      return p + (c.isChecked ? c.price * c.count : 0);
    }, 0);
    // 计算购买了多少件商品
    const totalCount = cartList.reduce((p, c) => {
      return p + (c.isChecked ? c.count : 0);
    }, 0);

    this.setData({
      cartList,
      isAllCheck,
      totalPrice,
      totalCount,
    });
  },

  // 单选功能
  checkOne: throttle(async function (e) {
    const { goodsid, ischecked } = e.currentTarget.dataset;
    await checkCartApi(goodsid, ischecked === 1 ? 0 : 1);
    this.getCartList();
  }, 500),

  // 全选功能
  allCheck: throttle(async function () {
    await checkAllCartApi(this.data.isAllCheck ? 0 : 1);
    this.getCartList();
  }, 500),

  // 删除一条数据
  delOne(e) {
    const { goodsid } = e.currentTarget.dataset;
    wx.showModal({
      content: "确认删除？",
      complete: async (res) => {
        if (res.confirm) {
          await delCartApi(goodsid);
          this.getCartList();
        }
      },
    });
  },

  updateCount: debounce(async function (e) {
    const { goodsid, count } = e.currentTarget.dataset;
    const newCount = e.detail;
    await upDateCartCountApi(goodsid, newCount - count);
    this.getCartList();
  }, 500),

  goDetail() {
    console.log("执行");
    wx.navigateTo({
      url: "/pages/order/detail/detail",
    });
  },
});
