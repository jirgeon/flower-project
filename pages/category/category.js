// pages/category/category.js
import { getCategoryListApi } from "../../api/category";

Page({
  /**
   * 页面的初始数据
   */
  data: {
    categoryList: [], // 分类列表数据
    categorySelectIndex: 0, // 当前选中的分类，默认值为 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getCategoryList();
  },

  // REQ 获取分类列表数据
  async getCategoryList() {
    const categoryList = await getCategoryListApi();
    this.setData({
      categoryList,
    });
  },

  setIndex(event) {
    const index = event.currentTarget.dataset.index;
    this.setData({
      categorySelectIndex: index,
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
