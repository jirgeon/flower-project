// pages/home/home.js
import {
  getBannerListApi,
  getNavListApi,
  getLikeGoodsListApi,
  getRecommendGoodsListApi,
} from "../../api/home";

Page({
  data: {
    bannerList: [],
    navList: [],
    likeGoodsList: [],
    recommendGoodsList: [],
  },

  /**
   * 生命周期函数--监听页面显示
   */
  async onShow() {
    this.getBannerList();
    this.getNavList();
    this.getLikeGoodsList();
    this.getRecommendGoodsList();
  },

  // REQ 获取猜你喜欢列表数据
  async getLikeGoodsList() {
    const likeGoodsList = await getLikeGoodsListApi();
    this.setData({
      likeGoodsList,
    });
  },

  // REQ 获取人气推荐列表数据
  async getRecommendGoodsList() {
    const recommendGoodsList = await getRecommendGoodsListApi();
    this.setData({
      recommendGoodsList,
    });
  },

  // REQ 获取轮播图数据
  async getBannerList() {
    const bannerList = await getBannerListApi();
    this.setData({
      bannerList: bannerList.sort((a, b) => {
        a.sort - b.sort;
      }),
    });
  },

  // REQ 获取导航列表数据
  async getNavList() {
    const navList = await getNavListApi();
    this.setData({
      navList,
    });
  },

  onShareAppMessage() {
    return {
      title: "情人节送礼",
      path: "/pages/home/home",
      imageUrl: "/static/images/1.png",
    };
  },

  onShareTimeline() {
    return {
      title: "同性交友",
      path: "/pages/home/home",
      imageUrl: "/static/images/1.png",
    };
  },
});
