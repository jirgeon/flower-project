import {
  getGoodsInfoByBuyNowApi,
  getGoodsInfoByTradeApi,
  submitOrderApi,
  createWeixinOrderApi,
} from "../../../api/pay";
import { getDefaultAddressApi } from "../../../api/address";
import { formatData } from "../../../utils/tools";

const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    buyName: "", // 购买人名称
    buyPhone: null, // 购买人手机号
    deliveryDate: "请选择配送时间", // 期望送达日期
    remarks: "", // 客户备注
    totoalPrice: "", // 总价
    cartList: [], // 购物车列表
    isShowPopup: false, // 日期选项卡显示与隐藏
    minDate: new Date().getTime(), // 可选最小时间格式
    currentDate: new Date().getTime(), // 当期时间
    goodsid: "",
    blessing: "",
    address: {},
  },

  handleShowPopup() {
    this.setData({
      isShowPopup: true,
    });
  },

  // 日期抽屉取消处理函数
  handleCancelTimePicker() {
    this.setData({
      isShowPopup: false,
    });
  },

  // 日期确认处理函数
  handleConfirmTimePicker(e) {
    const deliveryDate = formatData(e.detail);
    this.setData({
      isShowPopup: false,
      deliveryDate,
    });
  },

  onClose() {
    this.setData({
      isShowPopup: false,
    });
  },

  onLoad(options) {
    const { goodsid, blessing } = options;
    this.data.goodsid = goodsid;
    this.data.blessing = blessing;
  },

  onShow() {
    this.getTradeInfo();
    this.getAddress();
  },

  // 获取订单信息
  async getTradeInfo() {
    let res;
    const { goodsid, blessing } = this.data;
    if (goodsid) {
      // 表示从立即购买那边过来的
      res = await getGoodsInfoByBuyNowApi(goodsid, blessing);
    } else {
      res = await getGoodsInfoByTradeApi();
    }
    this.setData({
      cartList: res.cartVoList,
      totoalPrice: res.totalAmount,
    });
  },

  // 获取地址
  async getAddress() {
    let address = app.globalData.address;
    if (!address.id) {
      address = await getDefaultAddressApi();
    }
    this.setData({
      address,
    });
  },

  // 去结算
  async pay() {
    const {
      buyName,
      buyPhone,
      deliveryDate,
      cartList,
      remarks,
      address: { id },
    } = this.data;

    // 1. 将订单数据传递给服务端，生成订单，返回订单参数
    const orderNo = await submitOrderApi({
      buyName,
      buyPhone,
      deliveryDate,
      cartList,
      remarks,
      userAddressId: id,
    });

    // 2. 根据订单号获取 微信支付 需要的参数
    const payParams = await createWeixinOrderApi(orderNo);
    console.log(payParams);

    // 3. 使用微信支付接口进行支付
    wx.requestPayment({
      ...payParams,
      success() {
        console.log("支付成功！");
      },
      fail() {
        console.log("支付失败！");
      },
    });
  },
});
