import { getUserInfoApi } from "../../api/user";

const app = getApp();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 用户信息
    personInfo: {
      nickname: "",
      headimgurl: "",
    },
    defaultImg: "https://img02.hua.com/wxmp/hua/def_user_header.png",
  },

  onShow() {
    // 避免频繁请求
    if (!app.globalData.token || this.data.personInfo.nickname) return;
    this.getUserInfo();
  },

  async getUserInfo() {
    const result = await getUserInfoApi();
    console.log(result);

    this.setData({
      personInfo: {
        nickname: "华莱士",
        headimgurl: "./3.gif",
      },
    });
  },
});
