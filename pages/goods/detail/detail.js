import { getGoodsDetailInfoApi } from "../../../api/goods";

// pages/goods/detail/detail.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    goodsId: "",
    goodsDetail: {},
    isShow: false,
    blessing: "",
  },

  onLoad(options) {
    const goodsId = options.goodsId;
    this.setData({
      goodsId,
    });
  },

  onShow() {
    this.getGoodsDetailInfo();
  },

  // 获取商品详情数据
  async getGoodsDetailInfo() {
    const goodsDetail = await getGoodsDetailInfoApi(this.data.goodsId);
    this.setData({
      goodsDetail,
    });
  },

  // 回到首页
  goHome() {
    wx.switchTab({
      url: "/pages/home/home",
    });
  },

  // 立即购买
  buyNowHandler() {
    this.setData({
      isShow: true,
    });
  },

  // 确认购买跳转
  buyNowConfirmHandler(e) {
    const { goodsid } = e.currentTarget.dataset;
    wx.navigateTo({
      url: `/pages/order/detail/detail?goodsid=${goodsid}&blessing=${this.data.blessing}`,
    });
  },
});
