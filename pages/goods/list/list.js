import { getGoodsListApi } from "../../../api/goods";

// pages/goods/list/list.js
Page({
  data: {
    // 查询参数
    searchOptions: {
      page: 1,
      limit: 10,
      category1Id: "",
      category2Id: "",
    },
    // 商品数据
    goodsList: [],
    // 总页数 | 判断是否要下拉获取数据
    totalPage: 0,
    // true 表示正在刷新
    isRefreshing: false,
  },
  onLoad(options) {
    const { category1Id, category2Id } = options;
    // 视图无关数据，无需使用 setData
    this.data.searchOptions.category1Id = category1Id || "";
    this.data.searchOptions.category2Id = category2Id || "";
  },

  async getGoodsList() {
    const result = await getGoodsListApi(this.data.searchOptions);
    const goodsList =
      this.data.searchOptions.page === 1
        ? result.records
        : [...this.data.goodsList, ...result.records];
    this.setData({
      totalPage: result.pages,
      goodsList,
    });
  },

  // 回到首页
  goHome() {
    wx.switchTab({
      url: "/pages/home/home",
    });
  },

  // 下拉加载数据
  pullUpLoad() {
    // 判断是否到底部
    if (this.data.searchOptions.page >= this.data.totalPage) {
      return;
    }

    // 页数加 1
    this.setData({
      searchOptions: {
        ...this.data.searchOptions,
        page: this.data.searchOptions.page + 1,
      },
    });

    // 请求获取页面数据
    this.getGoodsList();
  },

  // 下拉刷新
  async pullDownRefresh() {
    this.setData({
      searchOptions: {
        ...this.data.searchOptions,
        page: 1,
      },
    });

    await this.getGoodsList();
    this.setData({
      isRefreshing: false,
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getGoodsList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
